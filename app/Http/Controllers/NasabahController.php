<?php

namespace App\Http\Controllers;

use App\Models\Nasabah;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NasabahController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $nasabah = Nasabah::all();
        return view('create_nasabah', compact('nasabah'));
    }

    public function seePoints()
    {
        $listNasabah = DB::select("SELECT n.accountId,n.name FROM `transaksis` join nasabahs as n on n.accountId = transaksis.account_id GROUP by account_id");

        $nasabahBeli = DB::select("SELECT n.accountId,t.* FROM `transaksis` as t join nasabahs as n on n.accountId = t.account_id WHERE n.accountId = t.account_id and description LIKE 'beli%'");

        $nasabahBayar = DB::select("SELECT n.name,n.accountId,t.* FROM `transaksis` as t join nasabahs as n on n.accountId = t.account_id WHERE n.accountId = 11 and description LIKE '%bayar%'");
        $getPoints = array('data' => [
        ]);
        foreach ($listNasabah as $key => $value) {
            array_push($getPoints['data'],[
                'accountId' => $value->accountId + 1,
                'name' => $value->name,
                'points' => [],
            ]);
        }
        dump($getPoints['data']);

        return view('nasabah_points', compact('nasabahBayar', 'listNasabah'));
    }
    public function calculatePoints($bayarListrikValue)
        {
            $points = 0;

            if ($bayarListrikValue > 100000) {
                // $points += floor(($bayarListrikValue - 100000) / 2000) * 2;
                $points += (($bayarListrikValue - 100000) / 2000) * 2;
                $bayarListrikValue = 100000;
            }

            if ($bayarListrikValue > 50000) {
                // $points += floor(($bayarListrikValue - 50000) / 2000);
                $points += (($bayarListrikValue - 50000) / 2000);
            }

            return $points;
        }

    public function nasabahPrint(Request $request)
    {
        $listNasabah = Nasabah::select('accountId')->get();
        $transaksi = NULL;
        if ($request->has('customerid')) {
            $transaksi = DB::select("SELECT n.accountId,t.transaction_date,t.description,t.debit_credit_status,t.amount FROM `transaksis` as t join nasabahs as n on n.accountId = t.account_id WHERE n.accountId = $request->customerid AND transaction_date BETWEEN '$request->start_date' AND '$request->end_date' ORDER BY t.transaction_date ASC");
        }
        return view('nasabah_prints', compact('transaksi', 'listNasabah'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->only('customername');
        Nasabah::create([
            'name' => $request->customername
        ]);
        return redirect()->back()->with('data', $data['customername']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Nasabah $nasabah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Nasabah $nasabah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Nasabah $nasabah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Nasabah $nasabah)
    {
        //
    }
}
