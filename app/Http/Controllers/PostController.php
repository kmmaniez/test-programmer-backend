<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    
    public function index()
    {
        $post = Post::all();
        return view('users.index', compact('post'));
    }

    
    public function create()
    {
        return view('users.create');
    }

    
    public function store(StorePostRequest $request)
    {
        $validated = $request->validated();
        $post = Post::all();
        if ($validated->fails()) {
            return redirect()->back()->with('errors', $validated->errors());
        }
        return view('users.create', compact('post'));
    }

    
    public function show(Post $post)
    {
        $post = Post::all();
        return view('users.detail', compact('post'));
    }

    
    public function edit(Post $post)
    {
        $post = Post::findOrFail('id', $post->id)->get();
        return view('users.edit', compact('post'));
    }

    
    public function update(StorePostRequest $request, Post $post)
    {
        Post::where('id', $post->id)->update([

        ]);
        return view('users.index', compact('post'));
    }

    
    public function destroy(Post $post)
    {
        try {
            $post->delete();
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', $th->getMessage());
        }
        return redirect(route('userss.index'))->with('message','Berhasil dihapus');
    }
}
