<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{

    public function authorize(): bool
    {
        return false;
    }

    public function rules(): array
    {
        return [
            'nama_lengkap' => 'required|unique:posts|max:255',
            'berat_badan' => 'required',
            'email' => 'required',
            'jenis_kelamin' => 'required',
            'tgl_lahir' => 'required'
        ];
    }
}
