<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    table,
    td,
    th {
        border: 1px solid #000;
        padding: 8px;
    }

    table {
        margin-top: 3rem;
        width: max-content;
        border-collapse: collapse;
    }

    .form-group {
        width: 164px;
        max-width: 280px;
        display: flex;
        flex-flow: column nowrap;
        margin-top: 16px;
    }

    .form-group-option label {
        display: block;
        margin-top: 16px;
    }

    .form-group-option {
        display: inline;
    }

    button {
        margin: 8px 0;
    }
</style>

<body>
    <h3>Filter Transaction</h3>
    <a href="{{ route('nasabah.index') }}" style="margin-top: 18px;">Tambah data nasabah</a>
    <a href="{{ route('nasabah.points') }}" style="margin-top: 18px;">Lihat poin transaksi</a>
    
    <form action="" method="get">
        {{-- @csrf --}}
        <div class="form-group">
            <label for="customerid">Customer ID</label>
            <span>
                Available id @foreach ($listNasabah as $data)
                    {{ $data['accountId'] }},
                @endforeach
            </span>
            <input type="number" min="1" name="customerid" id="customerid">
        </div>
        <div class="form-group">
            <label for="start_date">Start date</label>
            <input type="date" name="start_date" id="start_date">
        </div>
        <div class="form-group">
            <label for="end_date">End date</label>
            <input type="date" name="end_date" id="end_date">
        </div>
        <button type="submit">Filter</button>
    </form>
    @if (request()->has('customerid'))
        <p>CustomerID <strong>{{ request()->get('customerid') }}</strong> | Start date : {{ request()->get('start_date') }}, End date : {{ request()->get('end_date') }}</p>
    @endif
    <table>
        <thead>
            <tr>
                <th>Transaction Date</th>
                <th>Description</th>
                <th>Credit</th>
                <th>Debit</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @if (empty($transaksi))
                <tr>
                    <td colspan="5">Tidak ada data</td>
                </tr>
            @else
                @foreach ($transaksi as $data)
                    <tr>
                        <td>{{ $data->transaction_date }}</td>
                        <td>{{ $data->description }}</td>
                        <td>
                            @if ($data->debit_credit_status === 'C')
                                {{ number_format($data->amount, 0, ',', '.') }}
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if ($data->debit_credit_status === 'D')
                                {{ number_format($data->amount, 0, ',', '.') }}
                            @else
                                -
                            @endif
                        </td>
                        <td>{{ number_format($data->amount, 0, ',', '.') }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</body>

</html>
