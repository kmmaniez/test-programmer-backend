<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    table,
    td,
    th {
        border: 1px solid #000;
        padding: 8px;
    }

    table {
        margin-top: 3rem;
        width: max-content;
        border-collapse: collapse;
    }

    .form-group {
        width: 164px;
        max-width: 280px;
        display: flex;
        flex-flow: column nowrap;
        margin-top: 16px;
    }

    .form-group-option label {
        display: block;
        margin-top: 16px;
    }

    .form-group-option {
        display: inline;
    }

    button {
        margin: 8px 0;
    }
</style>

<body>
    <h3>Create Transaction</h3>
    <a href="{{ route('nasabah.index') }}" style="margin-top: 18px;">Tambah data nasabah</a>
    <a href="{{ route('nasabah.points') }}" style="margin-top: 18px;">Lihat poin transaksi</a>
    <a href="{{ route('nasabah.prints') }}" style="margin-top: 18px;">Cetak transaksi</a>
    
    <form action="{{ route('transaksi.store') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="customerid">Customer ID</label>
            <span>
                Available id 
                @foreach ($listNasabah as $data)
                   {{ $data['accountId'] }},
                @endforeach
            </span>
            <input type="number" min="1" name="customerid" id="customerid">
        </div>
        <div class="form-group">
            <label for="transactdate">Transaction date</label>
            <input type="date" name="transactdate" id="transactdate">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" name="description" id="description">
        </div>
        <div class="form-group-option">
            <label for="payment_option">Debit or credit</label>
            <input type="radio" name="payment_option" id="payment_option" value="D">Debit
            <input type="radio" name="payment_option" id="payment_option" value="C">Credit
        </div>
        <div class="form-group">
            <label for="money_amount">Money amount</label>
            <input type="number" min="0" name="money_amount" id="money_amount">
        </div>

        <button type="submit">Save</button>
    </form>
    
    <table>
        <thead>
            <tr>
                <th>AccountId</th>
                <th>Transaction Date</th>
                <th>Description</th>
                <th>Debit or Credit</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($transaksi as $data)
                <tr>
                    <td>{{ $data->account_id }}</td>
                    <td>{{ $data->transaction_date }}</td>
                    <td>{{ $data->description }}</td>
                    <td>{{ $data->debit_credit_status }}</td>
                    <td>{{ number_format($data->amount, 0, ',', '.') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
