<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    table,
    td,
    th {
        border: 1px solid #000;
        padding: 8px;
    }

    table {
        margin-top: 3rem;
        width: 200px;
        border-collapse: collapse;
    }
</style>

<body>
    <a href="{{ route('transaksi.index') }}" style="margin-top: 18px;">Tambah data transaksi</a>
    
    <table>
        <thead>
            <tr>
                <th>AccountId</th>
                <th>Name</th>
                <th>Total Point</th>
            </tr>
        </thead>
        <tbody>
            
            @forelse ($listNasabah as $data)
                <tr>
                    <td>{{ $data->accountId }}</td>
                    <td>{{ $data->name }}</td>
                    <td>point</td>
                </tr>
            @empty
                <tr>
                    <td colspan="2">Data kosong</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</body>

</html>
