<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->id();
        $table->foreignId('account_id')->references('accountId')->on('nasabahs')->cascadeOnDelete()->cascadeOnUpdate();
            $table->date('transaction_date');
            $table->string('description');
            $table->enum('debit_credit_status',['D','C']);
            $table->integer('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transaksis');
    }
};
