<?php

use App\Http\Controllers\NasabahController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\TransaksiController;
use App\Models\Nasabah;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect(route('nasabah.index'));
});

Route::resource('users', PostController::class);

Route::controller(NasabahController::class)->group(function(){
    Route::get('/nasabah','index')->name('nasabah.index');
    Route::post('/nasabah/store','store')->name('nasabah.store');

    Route::get('/nasabah/points', 'seePoints')->name('nasabah.points');
    
    Route::get('/nasabah/prints', 'nasabahPrint')->name('nasabah.prints');
    Route::post('/nasabah/prints', 'nasabahPrint')->name('nasabah.prints');
});

// Route::resource('nasabah',NasabahController::class)->except('show');
Route::resource('transaksi',TransaksiController::class)->except('show');
